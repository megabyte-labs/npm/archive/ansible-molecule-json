const COMMAND = 'ansible-molecule-json'
export const CLI_OPTIONS = {
  append: 'Version 0.0.1',
  options: [
    {
      alias: 'h',
      description: 'Displays help',
      option: 'help',
      type: 'Boolean',
    },
    {
      alias: 'd',
      description: 'Relative path to the data file.',
      example: `${COMMAND} --data ./data.json`,
      option: 'data',
      type: 'String',
    },
    {
      alias: 'o',
      default: './ansible-molecule.json',
      description: 'Relative path to the output file',
      example: `${COMMAND} --output ./ansible-molecule.json`,
      option: 'output',
      type: 'String',
    },
    {
      alias: 'c',
      description: 'Additional commands to run before analyzing',
      example: `${COMMAND} --command "molecule test"`,
      option: 'command',
      type: 'String',
    },
    {
      alias: 'i',
      description: 'Inject output to a JSON file',
      example: `${COMMAND} --inject ./.blueprint.json`,
      option: 'inject',
      type: 'String',
    },
  ],
  prepend: `Usage: ${COMMAND} [options]`,
}
