/* eslint-disable no-console */
import { execSync } from 'child_process'
import path from 'path'
import readline from 'readline'
import { promisify } from 'util'

import _optionator from 'optionator'

import { App } from './app'
import { CLI_OPTIONS } from './constants/cli-options.constant'
import { Logger } from './lib/log'

const optionator = _optionator(CLI_OPTIONS)

const EXIT_0 = 0
const EXIT_1 = 1

type ILooseObject = {
  readonly [key: string]: readonly string[]
}

/**
 * Verify enough command line options have provided
 *
 * @param {ILooseObject} options command line arguments
 * @returns {boolean} Returns true if enough arguments provided or false otherwise
 */
function noOptionsSet(options: ILooseObject): boolean {
  const optionsLength = Object.keys(options).length
  const minOptions = 2
  if (optionsLength < minOptions) {
    return true
  }
  if (optionsLength === minOptions && options.output && options._) {
    return true
  }

  return false
}

/**
 * This is where the logic that handles the CLI should reside
 *
 * @param {any} argz The command line arguments passed to the program
 * @returns {Promise<number>} Returns exit status
 */
export async function cli(argz: unknown): Promise<number> {
  let options
  try {
    options = optionator.parseArgv(argz)
  } catch (error) {
    // pretty print errors
    console.log(error.message)
    console.log('')

    return EXIT_1
  }

  if (options.help || noOptionsSet(options)) {
    console.log(optionator.generateHelp())

    return EXIT_0
  }

  if (!options.data) {
    console.log('--data option is required')
    console.log('')
    console.log(optionator.generateHelp())

    return EXIT_1
  }

  // pre commands?
  if (options.command) {
    try {
      Logger.info(`Command started. ${options.command}`)
      execSync(options.command, { stdio: 'inherit' })
    } catch (error) {
      Logger.error(error)

      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
      })
      const question = promisify(rl.question).bind(rl)
      try {
        const answer = await question('\n\nDo you want to continue ansible-molecule-json? (yes/no): ')
        if (/^n(o)?$/i.test(answer)) {
          return EXIT_1
        }
      } catch {
        return EXIT_1
      } finally {
        rl.close()
      }
    }
  }

  const inputFilePath = path.resolve(process.cwd(), options.data)

  try {
    const app = new App()
    const analyzedData = await app.run(inputFilePath)

    if (options.inject) {
      const injectFilePath = path.resolve(process.cwd(), options.inject)
      app.injectToFile(analyzedData, injectFilePath)
    } else {
      const outputFilePath = path.resolve(process.cwd(), options.output)
      app.outputToFile(analyzedData, outputFilePath)
    }

    return EXIT_0
  } catch (error) {
    Logger.error(error)

    return EXIT_1
  }
}
