import fs from 'fs'
import path from 'path'

import jsonfile from 'jsonfile'

/**
 * Input file validate
 *
 * @param {string} filePath Absolute file path fo the input file
 * @returns {true} true if valid
 * @throws Error if invalid
 */
export function validateInputFile(filePath): true {
  // is path exist?
  if (!fs.existsSync(filePath)) {
    throw new Error(`The path to the data file you specified (${filePath}) does not exist`)
  }

  // is path directory?
  if (fs.lstatSync(filePath).isDirectory()) {
    throw new Error('The path to the data file you specified appears to be a directory')
  }

  return true
}

/**
 * Validate output file path
 *
 * @param {string} filePath Absolute output file path
 * @returns {true} true if output file path is valid
 * @throws Error
 */
export function validateOutputFile(filePath: string): true {
  const baseDirectory = path.dirname(filePath)
  try {
    fs.accessSync(baseDirectory, fs.constants.W_OK)
  } catch {
    throw new Error('Output directory is not writable')
  }

  return true
}

/**
 * Validate target file
 *
 * @param {string} filePath Absolute path to the target file
 * @returns {true} Returns true if filePath is valid
 * @throws Error if validation fails
 */
export function validateInjectFile(filePath: string): true {
  // is path exist?
  if (!fs.existsSync(filePath)) {
    throw new Error(`The path to the data file you specified (${filePath}) does not exist`)
  }

  // is path directory?
  if (fs.lstatSync(filePath).isDirectory()) {
    throw new Error('The path to the data file you specified appears to be a directory')
  }

  // is json?
  // will throw error if not a valid JSON
  jsonfile.readFileSync(filePath)

  // is writable?
  try {
    fs.accessSync(filePath, fs.constants.W_OK)
  } catch {
    throw new Error('Target file is not writable')
  }

  return true
}
