import fs from 'fs'
import os from 'os'
import path from 'path'

import test, { ExecutionContext } from 'ava'
import stringify from 'json-stringify-pretty-compact'
import jsonfile from 'jsonfile'

import { IData, injectToFile } from './output'

// eslint-disable-next-line functional/prefer-readonly-type
const temporaryFiles: string[] = []

/**
 * Get file data
 *
 * @returns {string[][]} Returns data sample
 */
const getFileData = (): readonly (readonly string[])[] => [
  ['OS Family', 'OS Version', 'Status', 'Idempotent'],
  ['CentOS', '7', '✅', '❌'],
  ['CentOS', '8', '✅', '❌'],
  ['Debian', '9', '✅', '❌'],
  ['Debian', '10', '✅', '❌'],
  ['Ubuntu', '18.04', '✅', '❌'],
  ['Ubuntu', '20.04', '✅', '❌'],
]

/**
 * Get random file name
 *
 * @returns {string} random json file name
 */
// eslint-disable-next-line no-magic-numbers
const getRandomFileName = (): string => `${(Math.random().toString(36) + Math.random().toString(36)).slice(2, 7)}.json`

/**
 * Create random file
 *
 * @param {any} data Data to include in a file
 * @returns {string} File path of the created file
 */
function createTemporaryFile(data: unknown): string {
  const temporaryDirectory = os.tmpdir()
  const temporaryFilePath = path.join(temporaryDirectory, '/', getRandomFileName())
  // eslint-disable-next-line functional/immutable-data
  temporaryFiles.push(temporaryFilePath)
  fs.writeFileSync(temporaryFilePath, stringify(data), { encoding: 'utf8' })

  return temporaryFilePath
}

/**
 * Assert file data
 *
 * @param {ExecutionContext} t ExecutionContext from the ava
 * @param {IData} expected expected data to assert
 * @param {string} filePath Full file path of the file
 */
async function assertFileData(t: ExecutionContext, expected: IData, filePath: string): Promise<void> {
  const actual = await jsonfile.readFile(filePath)
  t.deepEqual(actual, expected)
}

test('Test output with insert to blank file', async (t) => {
  const data = ''
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
    ['CentOS', '8', '✅', '❌'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['BSD', '13.0', '✅', '❌'],
      ['CentOS', '8', '✅', '❌'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test('Test output with insert to file with blank string', async (t) => {
  const data = '               '
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
    ['CentOS', '8', '✅', '❌'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['BSD', '13.0', '✅', '❌'],
      ['CentOS', '8', '✅', '❌'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test('Test output with insert to string file', async (t) => {
  const data = 'this is a string'
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
    ['CentOS', '8', '✅', '❌'],
  ]
  await t.throwsAsync(
    async () => {
      await injectToFile(filePath, 'compatibility', newData)
    },
    { instanceOf: TypeError, message: `${filePath} contains string data. Injection only support for valid JSON object` }
  )
})

test('Test output with insert to array JSON', async (t) => {
  const data = ['foo', 'bar']
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
    ['CentOS', '8', '✅', '❌'],
  ]
  await t.throwsAsync(
    async () => {
      await injectToFile(filePath, 'compatibility', newData)
    },
    { instanceOf: TypeError, message: `${filePath} contains array data. Injection only support for valid JSON object` }
  )
})

test('Test output with insert to null', async (t) => {
  // eslint-disable-next-line unicorn/no-null
  const filePath = createTemporaryFile(null)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
    ['CentOS', '8', '✅', '❌'],
  ]
  await injectToFile(filePath, 'compatibility', newData)
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['BSD', '13.0', '✅', '❌'],
      ['CentOS', '8', '✅', '❌'],
    ],
  }
  await assertFileData(t, expectedData, filePath)
})

test('Test output with insert', async (t) => {
  const data = { compatibility: [] }
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
    ['CentOS', '8', '✅', '❌'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['BSD', '13.0', '✅', '❌'],
      ['CentOS', '8', '✅', '❌'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test('Test output with insert alphabetically 1', async (t) => {
  const data = { compatibility: getFileData() }
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['BSD', '13.0', '✅', '❌'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['BSD', '13.0', '✅', '❌'],
      ['CentOS', '7', '✅', '❌'],
      ['CentOS', '8', '✅', '❌'],
      ['Debian', '9', '✅', '❌'],
      ['Debian', '10', '✅', '❌'],
      ['Ubuntu', '18.04', '✅', '❌'],
      ['Ubuntu', '20.04', '✅', '❌'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test('Test output with insert alphabetically 2', async (t) => {
  const data = { compatibility: getFileData() }
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['Fedora', '9', '✅', '❌'],
    ['Windows', '11', '✅', '❌'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['CentOS', '7', '✅', '❌'],
      ['CentOS', '8', '✅', '❌'],
      ['Debian', '9', '✅', '❌'],
      ['Debian', '10', '✅', '❌'],
      ['Fedora', '9', '✅', '❌'],
      ['Ubuntu', '18.04', '✅', '❌'],
      ['Ubuntu', '20.04', '✅', '❌'],
      ['Windows', '11', '✅', '❌'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test('Test output with merge latest', async (t) => {
  const data = { compatibility: getFileData() }
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['Ubuntu', '18.04', '❌', '❌'],
    ['CentOS', '8', '✅', '✅'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['CentOS', '7', '✅', '❌'],
      ['CentOS', '8', '✅', '✅'],
      ['Debian', '9', '✅', '❌'],
      ['Debian', '10', '✅', '❌'],
      ['Ubuntu', '18.04', '❌', '❌'],
      ['Ubuntu', '20.04', '✅', '❌'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test('Test output with merge latest and insert', async (t) => {
  const data = { compatibility: getFileData() }
  const filePath = createTemporaryFile(data)
  const newData = [
    ['OS Family', 'OS Version', 'Status', 'Idempotent'],
    ['Ubuntu', '18.04', '❌', '❌'],
    ['Windows', '9', '✅', '✅'],
    ['CentOS', '8', '✅', '✅'],
  ]
  const expectedData = {
    compatibility: [
      ['OS Family', 'OS Version', 'Status', 'Idempotent'],
      ['CentOS', '7', '✅', '❌'],
      ['CentOS', '8', '✅', '✅'],
      ['Debian', '9', '✅', '❌'],
      ['Debian', '10', '✅', '❌'],
      ['Ubuntu', '18.04', '❌', '❌'],
      ['Ubuntu', '20.04', '✅', '❌'],
      ['Windows', '9', '✅', '✅'],
    ],
  }
  await injectToFile(filePath, 'compatibility', newData)
  await assertFileData(t, expectedData, filePath)
})

test.after.always('cleaning up', (): void => {
  // delete tempFiles
  if (temporaryFiles.length === 0) {
    return
  }
  temporaryFiles.map((file) => {
    fs.unlink(file, (error) => {
      if (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    })
  })
})
