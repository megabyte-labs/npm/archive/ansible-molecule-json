import { format } from 'date-fns'

import { IOS, IRecapDataResponse, IResults } from '../types'

import { sortResults } from './utils'

/**
 * Analyze output data from the Ansible Molecule
 */
export class TestDataAnalyzer {
  /**
   * Start analyzing recap data from the Ansible Molecule result
   *
   * @param {string[]} recapData Recap data from the analyzed data
   * @param {string[]} idempotenceRecapData Idempotent data from the analyzed data
   * @returns {string[][]} Returns merged data of both recap data and idempotent data
   */
  public static run(
    recapData: readonly string[],
    idempotenceRecapData: readonly string[]
  ): readonly (readonly string[])[] {
    // now analyze the buffers
    const initialTest = TestDataAnalyzer.parseRecap(recapData)
    const idempotenceTest = TestDataAnalyzer.parseRecap(idempotenceRecapData, true)

    // merge initial test with idempotence test
    const merged = TestDataAnalyzer.merge(initialTest, idempotenceTest)

    return merged
  }

  /**
   * Extract OS data from the given string
   *
   * @param {string} osData OS Data
   * @returns {IOS} Parsed OS data from the given data string
   */
  private static extractOS(osData: string): IOS {
    const matches = /\[\d+\w([^[]+)\[/.exec(osData)
    if (!matches) {
      throw new Error('Unable to determine the OS family')
    }
    const [family, version] = matches[1].split('-')

    return { family, key: matches[1].toLowerCase(), version: version || 'latest' }
  }

  /**
   * Extract results as a javascript object from the given string
   *
   * @param {string} stringData String to extract data
   * @returns {IResults|undefined} Returns undefined if no matches found. Otherwise returns the results
   */
  private static extractResults(stringData: string): IResults | undefined {
    const matches = /.*(ok=\d+).*(changed=\d+).*(unreachable=\d+).*(failed=\d+).*(skipped=\d+).*(rescued=\d+).*(ignored=\d+)/.exec(
      stringData
    )
    if (matches === null) {
      return
    }

    return {
      changed: Number.parseInt(matches[2].replace('changed=', ''), 10),
      failed: Number.parseInt(matches[4].replace('failed=', ''), 10),
      ignored: Number.parseInt(matches[7].replace('ignored=', ''), 10),
      ok: Number.parseInt(matches[1].replace('ok=', ''), 10),
      rescued: Number.parseInt(matches[6].replace('rescued=', ''), 10),
      skipped: Number.parseInt(matches[5].replace('skipped=', ''), 10),
      unreachable: Number.parseInt(matches[3].replace('unreachable=', ''), 10),
    }
  }

  /**
   * Get status string
   *
   * @param {IResults} data Result data to generate status for
   * @returns {string} Generated status
   */
  private static getStatus(data: IResults): string {
    const FAILED = 0
    if (data.failed > FAILED) {
      return '❌'
    }

    const NO_CHANGES = 0
    if (
      data.ok === NO_CHANGES &&
      data.changed === NO_CHANGES &&
      data.ignored === NO_CHANGES &&
      data.rescued === NO_CHANGES &&
      data.skipped === NO_CHANGES &&
      data.unreachable === NO_CHANGES
    ) {
      return '❓'
    }

    return '✅'
  }

  /**
   * Get status for idempotent
   *
   * @param {IResults} data Results data
   * @returns {string} Idempotent status
   */
  private static getIdempotentStatus(data: IResults): string {
    const others = new Set(['changed', 'unreachable', 'failed', 'rescued', 'ignored'])
    const ERRORS = 0
    if (Object.keys(data).some((key) => others.has(key) && data[key] > ERRORS)) {
      return '❌'
    }

    if (data.ok > ERRORS || data.skipped > ERRORS) {
      return '✅'
    }

    return '❓'
  }

  /**
   * Parse recap data from the ansible molecule output
   *
   * @param {string[]} recapData Recap data
   * @param {boolean} idempotence Is idempotent data to parse
   * @returns {IRecapDataResponse} RecapData
   */
  private static parseRecap(recapData: readonly string[], idempotence = false): IRecapDataResponse {
    const data = recapData.reduce((accumulator, line) => {
      // find the os
      const [osData, resultData] = line.split(':')
      const os = TestDataAnalyzer.extractOS(osData)
      if (!os) {
        throw new Error(`Unable to parse os data: ${osData}`)
      }
      const results = TestDataAnalyzer.extractResults(resultData)
      if (!results) {
        throw new Error(`Unable to parse results: ${resultData}`)
      }

      // eslint-disable-next-line functional/immutable-data
      accumulator[os.key] = {
        family: os.family,
        version: os.version,
      }

      if (idempotence) {
        // eslint-disable-next-line functional/immutable-data
        accumulator[os.key as string].idempotent = TestDataAnalyzer.getIdempotentStatus(results)
      } else {
        // eslint-disable-next-line functional/immutable-data
        accumulator[os.key].status = TestDataAnalyzer.getStatus(results)
      }

      return accumulator
    }, {})

    return data
  }

  /**
   * Merge initial test results and idempotent test result
   *
   * @param {IRecapDataResponse} initialTest Initial Recap data
   * @param {IRecapDataResponse} idempotenceTest idempotent Recap data
   * @returns {string[][]} compatibility as JSON object
   */
  private static merge(
    initialTest: IRecapDataResponse,
    idempotenceTest: IRecapDataResponse
  ): readonly (readonly string[])[] {
    const compatibility = Object.keys(initialTest).map((key) => {
      return [
        initialTest[key].family,
        initialTest[key]?.version || '❓',
        initialTest[key].status as string,
        (idempotenceTest[key] && (idempotenceTest[key].idempotent as string)) || '❓',
        format(new Date(), 'MMMM do, yyyy'),
      ]
    })

    const compatibilityWithHeader = [
      ['OS Family', 'OS Version', 'Status', 'Idempotent', 'Last Tested'],
      ...compatibility,
    ]

    const compatibilityWithHeaderSorted = sortResults(compatibilityWithHeader)

    return compatibilityWithHeaderSorted
  }
}
