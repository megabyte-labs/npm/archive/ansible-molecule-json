/* eslint-disable unicorn/prefer-module */
import path from 'path'

import test from 'ava'

import { validateInputFile, validateOutputFile } from './validators'

test('validateInputFile validator test', (t) => {
  const filePathNotExist = path.resolve(__dirname, './file-does-not-exists')
  t.throws(
    () => {
      validateInputFile(filePathNotExist)
    },
    { instanceOf: Error, message: `The path to the data file you specified (${filePathNotExist}) does not exist` }
  )
  t.notThrows(() => {
    const packagePath = path.resolve(__dirname, '../../../package.json')
    validateInputFile(packagePath)
  })
  t.throws(
    () => {
      validateInputFile(path.resolve(__dirname))
    },
    { instanceOf: Error, message: 'The path to the data file you specified appears to be a directory' }
  )
})

test('validateOutputFile validator test', (t) => {
  t.notThrows(() => {
    const outputFilePath = '/tmp/output.json'
    validateOutputFile(outputFilePath)
  })
  t.throws(
    () => {
      const outputFilePath = '/tmp/directory-does-not-exists/output.json'
      validateOutputFile(outputFilePath)
    },
    { instanceOf: Error, message: 'Output directory is not writable' }
  )
})
