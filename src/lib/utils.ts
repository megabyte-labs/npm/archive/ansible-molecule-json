/**
 * Sort results
 *
 * @param {string[][]} data data to sort
 * @returns {string[][]} Returns the sorted results
 */
export function sortResults(data: readonly (readonly string[])[]): readonly (readonly string[])[] {
  const temporary = [...data]
  // remove header
  // eslint-disable-next-line functional/immutable-data
  const header = temporary.shift()

  // sort
  // eslint-disable-next-line functional/immutable-data
  temporary.sort((a, b) => {
    const compared = a[0].localeCompare(b[0])
    if (compared === 0) {
      return a[1].toString().localeCompare(b[1].toString(), undefined, { numeric: true, sensitivity: 'base' })
    }

    return compared
  })

  // insert header again as the first
  // eslint-disable-next-line functional/immutable-data
  temporary.unshift(header as readonly string[])

  return temporary
}
