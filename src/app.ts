import { Logger } from './lib/log'
import { asJSONFile, injectToFile } from './lib/output'
import { TestDataAnalyzer } from './lib/test-data-analyzer'
import { TestDataLoader } from './lib/test-data-loader'
import { validateInjectFile, validateInputFile, validateOutputFile } from './lib/validators'

/**
 * This is where the app's logic should reside.
 *
 * See https://gitlab.com/megabyte-space/npm/buildr for examples
 */
export class App {
  /**
   * Start analyzing
   *
   * @param {string} filePath absolute file path
   * @returns {string[]} Parsed test runs
   */
  public async run(filePath: string): Promise<readonly (readonly string[])[]> {
    validateInputFile(filePath)

    Logger.info('Starting to read data file')
    const loader = new TestDataLoader()
    const { recapData, idempotenceRecapData } = await loader.load(filePath)

    Logger.info('Starting to analyze collected data')

    return TestDataAnalyzer.run(recapData, idempotenceRecapData)
  }

  /**
   * Inject the analyzed data to a JSON file
   *
   * @param {string[][]} data data to inject into file
   * @param {string} filePath Absolute file path to the target file
   */
  public injectToFile(data: readonly (readonly string[])[], filePath: string): void {
    // Validate
    validateInjectFile(filePath)

    Logger.info(`Injecting to ${filePath}`)
    injectToFile(filePath, 'compatibility', data)
  }

  /**
   * Output the given data to a file as JSON
   *
   * @param {string[][]} data Data to write into file
   * @param {string} filePath Absolute file path to the file
   */
  public outputToFile(data: readonly (readonly string[])[], filePath: string): void {
    // Validate
    validateOutputFile(filePath)

    Logger.info(`Writing output to ${filePath}`)
    asJSONFile(filePath, {
      compatibility: data,
    })
  }
}
