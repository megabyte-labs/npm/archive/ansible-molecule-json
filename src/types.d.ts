export type IOS = {
  readonly key: string
  readonly family: string
  readonly version: string | number
}

export type IResults = {
  readonly ok: number
  readonly changed: number
  readonly unreachable: number
  readonly failed: number
  readonly skipped: number
  readonly rescued: number
  readonly ignored: number
}

export type IRecapData = {
  readonly family: string
  readonly version: string

  readonly status?: string

  readonly idempotent?: string
}

export type IRecapDataResponse = {
  readonly [key: string]: IRecapData
}
