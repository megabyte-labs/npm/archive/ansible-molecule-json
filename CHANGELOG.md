# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/compare/v0.0.2...v0.0.3) (2021-06-02)

### 0.0.2 (2021-05-24)


### Features

* **analyzer:** improved analyzer component ([b15f62f](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/commit/b15f62f8aa32befa4fe620e0a58e588d3e6a8eba)), closes [#1](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/issues/1)
* **output:** added intelligence to the inject function ([ed0d711](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/commit/ed0d711596640acc72ee0a95fc8d36516e399c30))
* completed ansible-molecule-json functionality ([1f822d8](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/commit/1f822d8fb4bb804a552564ce05a43951d00b6918)), closes [#1](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/issues/1) [#1](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/issues/1)


### Bug Fixes

* **cli:** improved cli error handling ([b496214](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/commit/b4962144d7156312ea3a5d055a1e291e822d4c21)), closes [#1](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/issues/1)
