<!-- ⚠️ This README has been generated from the file(s) "./.modules/docs/blueprint-readme.md" ⚠️--><div align="center">
  <center>
    <a href="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json" title="@megabytelabs/ansible-molecule-json GitLab page" target="_blank">
      <img width="100" height="100" alt="@megabytelabs/ansible-molecule-json logo" src="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/raw/master/logo.png" />
    </a>
  </center>
</div>
<div align="center">
  <center><h1 align="center">NPM Package: Ansible Molecule JSON</h1></center>
</div>

<div align="center">
  <h4 align="center">
    <a href="https://megabyte.space" title="Megabyte Labs homepage" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/home-solid.svg" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansible-molecule-json" title="@megabytelabs/ansible-molecule-json package on npmjs.org" target="_blank">
      <img height="50" src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/npm.svg" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/blob/master/CONTRIBUTING.md" title="Learn about contributing" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/contributing-solid.svg" />
    </a>
    <a href="https://www.patreon.com/ProfessorManhattan" title="Support us on Patreon" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/support-solid.svg" />
    </a>
    <a href="https://app.slack.com/client/T01ABCG4NK1/C01NN74H0LW/details/" title="Slack chat room" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/chat-solid.svg" />
    </a>
    <a href="https://github.com/ProfessorManhattan/npm-ansible-molecule-json" title="GitHub mirror" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/github-solid.svg" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json" title="GitLab repository" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/gitlab-solid.svg" />
    </a>
  </h4>
  <p align="center">
    <a href="https://www.npmjs.com/package/@megabytelabs/ansible-molecule-json" target="_blank">
      <img alt="Version: 0.0.3" src="https://img.shields.io/badge/version-0.0.3-blue.svg?cacheSeconds=2592000&style=badge_style" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/commits/master" title="GitLab CI build status" target="_blank">
      <img alt="Build status" src="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/badges/master/pipeline.svg">
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansible-molecule-json" title="Dependency status reported by Depfu">
      <img alt="Dependency status reported by Depfu" src="https://img.shields.io/depfu/megabyte-labs/npm-ansible-molecule-json?style=badge_style&logo=npm" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansible-molecule-json" title="Zip file size">
      <img alt="Zip file size" src="https://img.shields.io/bundlephobia/minzip/@megabytelabs/ansible-molecule-json?style=bad_style&logo=npm" />
    </a>
    <a href="" title="Total downloads of @megabytelabs/ansible-molecule-json on npmjs.org">
      <img alt="Total downloads of @megabytelabs/ansible-molecule-json on npmjs.org" src="https://img.shields.io/npm/dt/@megabytelabs/ansible-molecule-json?logo=npm&style=badge_style&logo=npm" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansible-molecule-json" title="Number of vulnerabilities from Snyk scan on @megabytelabs/ansible-molecule-json">
      <img alt="Number of vulnerabilities from Snyk scan on @megabytelabs/ansible-molecule-json" src="https://img.shields.io/snyk/vulnerabilities/npm/@megabytelabs/ansible-molecule-json?style=badge_style&logo=npm" />
    </a>
    <a href="https://megabyte.space/docs/npm" target="_blank">
      <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg?logo=readthedocs&style=badge_style" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/raw/master/LICENSE" target="_blank">
      <img alt="License: MIT" src="https://img.shields.io/badge/license-MIT-yellow.svg?style=badge_style" />
    </a>
    <a href="https://opencollective.com/megabytelabs" title="Support us on Open Collective" target="_blank">
      <img alt="Open Collective sponsors" src="https://img.shields.io/opencollective/sponsors/megabytelabs?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAElBMVEUAAACvzfmFsft4pfD////w+P9tuc5RAAAABHRSTlMAFBERkdVu1AAAAFxJREFUKM9jgAAXIGBAABYXMHBA4yNEXGBAAU2BMz4FIIYTNhtFgRjZPkagFAuyAhGgHAuKAlQBCBtZB4gzQALoDsN0Oobn0L2PEUCoQYgZyOjRQFiJA67IRrEbAJImNwFBySjCAAAAAElFTkSuQmCC&label=Open%20Collective%20sponsors&style=badge_style" />
    </a>
    <a href="https://github.com/ProfessorManhattan" title="Support us on GitHub" target="_blank">
      <img alt="GitHub sponsors" src="https://img.shields.io/github/sponsors/ProfessorManhattan?label=GitHub%20sponsors&logo=github&style=badge_style" />
    </a>
    <a href="https://github.com/ProfessorManhattan" target="_blank">
      <img alt="GitHub: ProfessorManhattan" src="https://img.shields.io/github/followers/ProfessorManhattan?style=social" target="_blank" />
    </a>
    <a href="https://twitter.com/MegabyteLabs" target="_blank">
      <img alt="Twitter: MegabyteLabs" src="https://img.shields.io/twitter/url/https/twitter.com/MegabyteLabs.svg?style=social&label=Follow%20%40MegabyteLabs" />
    </a>
  </p>
</div>

> </br><h3 align="center">**An npm package that converts the terminal output from an Ansible Molecule test into a JSON compatibility matrix**</h3></br>

<!--TERMINALIZER![terminalizer_title](https://gitlab.com/megabyte-labs/npm/role_name/-/raw/master/.demo.gif)TERMINALIZER-->

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#table-of-contents)

## ➤ Table of Contents

- [➤ Requirements](#-requirements)
- [➤ Overview](#-overview)
- [➤ Getting Started](#-getting-started)
  - [Example Usage](#example-usage)
  - [Example Output](#example-output)
  - [Example Input](#example-input)
- [➤ Ansible Molecule Example](#-ansible-molecule-example)
- [➤ Contributing](#-contributing)
- [➤ License](#-license)

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#requirements)

## ➤ Requirements

- **[Node.js >9](https://gitlab.com/megabyte-labs/ansible-roles/nodejs)**

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#overview)

## ➤ Overview

Ansible is a tool that allows you to provision machines over SSH. When developing with Ansible, it is common to support multiple operating systems. Testing multiple operating systems after making changes to an Ansible role can be time consuming. In order to help developers get over this hurdle, Ansible provides the [Molecule](https://github.com/ansible-community/molecule) testing platform. Molecule helps you test your Ansible roles against multiple environments at the same time. Sadly, Molecule does not support the ability to output the results in JSON format.

This NPM package converts Ansible Molecule test data into JSON format. We personally use this to embed compatibility matrices into our README.md files across [hundreds of Ansible roles](https://gitlab.com/megabyte-labs/ansible-roles). After piping the output of your Ansible Molecule test to a text file, you can then use this NPM package to convert the data into JSON format (for consumption by other applications). The JSON output is formatted to be compatible with [@appnest/readme](https://github.com/andreasbm/readme) which we utilize to generate our documentation using partials.

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#getting-started)

## ➤ Getting Started

You can run `ansible-molecule-json` by installing it globally or running it with `npx`. You can run `ansible-molecule-json --help` to see the available parameters. You should see output that looks something like this:

```shell
❯ ansible-molecule-json --help
Usage: ansible-molecule-json [options]

  -h, --help            Displays help
  -d, --data String     Relative path to the data file.
  -o, --output String   Relative path to the output file - default: ./ansible-molecule.json
  -c, --command String  Additional commands to run before analyzing
  -i, --inject String   Inject output to a JSON file
```

### Example Usage

1. Run the molecule test and pipe the output to a text file (e.g. `molecule test > molecule-output.txt`)
2. Run `npx ansible-molecule-json --data ./molecule-output.txt --output ./molecule-output.json`

### Example Output

The converter will generate the JSON in the following format:

```json
{
  "compatibility": [
    ["OS Family", "OS Version", "Status", "Idempotent"],
    ["Archlinux", null, "✅", "✅"],
    ["CentOS", 7, "✅", "✅"],
    ["CentOS", 8, "✅", "✅"],
    ["Debian", 10, "✅", "✅"],
    ["Debian", 9, "✅", "✅"],
    ["Fedora", 33, "✅", "✅"],
    ["Ubuntu", 18.04, "✅", "✅"],
    ["Ubuntu", 20.04, "✅", "✅"]
  ]
}
```

### Example Input

For debugging purposes, below is a snippet of the format that `ansible-molecule-json` is expecting. When you run `molecule test > molecule-output.txt`, you should see that the contents of `molecule-output.txt` looks something like this:

```
[34mINFO    [0m docker scenario test matrix: lint, dependency, cleanup, destroy, syntax, create, prepare, converge, idempotence, side_effect, verify, cleanup, destroy
[34mINFO    [0m [2;36mRunning [0m[2;32mdocker[0m[2;36m > [0m[2;32mlint[0m
[34mINFO    [0m Lint is disabled.
[34mINFO    [0m [2;36mRunning [0m[2;32mdocker[0m[2;36m > [0m[2;32mdependency[0m
[31mWARNING [0m Skipping, missing the requirements file.
[31mWARNING [0m Skipping, missing the requirements file.
[34mINFO    [0m [2;36mRunning [0m[2;32mdocker[0m[2;36m > [0m[2;32mcleanup[0m
[31mWARNING [0m Skipping, cleanup playbook not configured.
[34mINFO    [0m [2;36mRunning [0m[2;32mdocker[0m[2;36m > [0m[2;32mdestroy[0m
[34mINFO    [0m Sanity checks: [32m'docker'[0m

PLAY [Destroy] *****************************************************************

TASK [Destroy molecule instance(s)] ********************************************
[33mchanged: [localhost] => (item=Archlinux-Latest)[0m
[33mchanged: [localhost] => (item=CentOS-7)[0m
[33mchanged: [localhost] => (item=CentOS-8)[0m
[33mchanged: [localhost] => (item=Debian-9)[0m
[33mchanged: [localhost] => (item=Debian-10)[0m
[33mchanged: [localhost] => (item=Fedora-33)[0m
[33mchanged: [localhost] => (item=Ubuntu-18.04)[0m
[33mchanged: [localhost] => (item=Ubuntu-20.04)[0m
```

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#ansible-molecule-example)

## ➤ Ansible Molecule Example

If you are new to Ansible Molecule and are looking for an example of how to integrate Ansible Molecule tests into your Ansible project, you might find our [Android Studio role](https://gitlab.com/megabyte-labs/ansible-roles/androidstudio) helpful. You can find the Ansible Molecule test scenarios in the `molecule/` folder. Each scenario is in its own folder. The default scenario (i.e. the scenario in the folder named `default/`) will run when you run `molecule test`. For all the other scenarios, you have to run `molecule test -s folder_name`. For more information, please see the Android Studio [README.md](https://gitlab.com/megabyte-labs/ansible-roles/androidstudio/-/blob/master/README.md) and [CONTRIBUTING.md](https://gitlab.com/megabyte-labs/ansible-roles/androidstudio/-/blob/master/CONTRIBUTING.md).

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#contributing)

## ➤ Contributing

Contributions, issues, and feature requests are welcome! Feel free to check the [issues page](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/issues). If you would like to contribute, please take a look at the [contributing guide](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/blob/master/CONTRIBUTING.md).

<details>
<summary>Sponsorship</summary>
<br/>
<blockquote>
<br/>
I create open source projects out of love. Although I have a job, shelter, and as much fast food as I can handle, it would still be pretty cool to be appreciated by the community for something I have spent a lot of time and money on. Please consider sponsoring me! Who knows? Maybe I will be able to quit my job and publish open source full time.
<br/><br/>Sincerely,<br/><br/>

**_Brian Zalewski_**<br/><br/>

</blockquote>

<a href="https://www.patreon.com/ProfessorManhattan">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

</details>

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#license)

## ➤ License

Copyright © 2021 [Megabyte LLC](https://megabyte.space). This project is [MIT](https://gitlab.com/megabyte-labs/npm/ansible-molecule-json/-/raw/master/LICENSE) licensed.
